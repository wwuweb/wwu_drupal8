<?php

/**
 * Trigger a cache clear after enabling the csp module and reverting active
 * configuration.
 */
function wwu_drupal8_post_update_csp_config_revert(&$sandbox) {
  // Intentionally left blank.
}

/**
 * Trigger a cache clear after enabling the blazy module.
 */
function wwu_drupal8_post_update_blazy_enable(&$sandbox) {
  // Intentionally left blank.
}
